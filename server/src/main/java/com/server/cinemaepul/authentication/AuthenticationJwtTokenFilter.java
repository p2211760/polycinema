package com.server.cinemaepul.authentication;

import java.io.IOException;

import com.server.cinemaepul.user.User;
import com.server.cinemaepul.user.UserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import io.jsonwebtoken.ExpiredJwtException;

@Component
public class AuthenticationJwtTokenFilter extends OncePerRequestFilter {

    @Autowired
    UserService userService;

    @Autowired
    JwtUtils jwtUtils;

    AuthenticationProvider authenticationProvider;



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
//        final String requestTokenHeader = request.getHeader("Authorization");
//        System.out.println(requestTokenHeader);
//        //System.out.println(jwtUtils);
//        jwtUtils = new JwtUtils();
//        String username = null;
//        String jwtToken = null;
//        // JWT Token is in the form "Bearer token". Remove Bearer word and get
//        // only the Token
//        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
//            jwtToken = requestTokenHeader.substring(7);
//            System.out.println("Token : " + jwtUtils.validateJwtToken(jwtToken));
//            try {
//                username = jwtUtils.getUserNameFromJwtToken(jwtToken);
//            } catch (IllegalArgumentException e) {
//                e.printStackTrace();
//                System.out.println("Unable to get JWT Token");
//            } catch (ExpiredJwtException e) {
//                System.out.println("JWT Token has expired");
//            }
//        } else {
//            logger.warn("JWT Token is not Bearer String");
//        }
//
//        // Once we get the token validate it.
//        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
//
//            User user = userService.loadByLogin(username);
//            // if token is valid configure Spring Security to manually set
//            // authentication
//            if (jwtUtils.validateJwtToken(jwtToken)) {
//
//                usernamePasswordAuthenticationToken2 usernamePasswordAuthenticationToken = new usernamePasswordAuthenticationToken2(
//                        user.getUsername(), user.getPassword());
//                usernamePasswordAuthenticationToken
//                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//                //System.out.println(((User)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
//                System.out.println((SecurityContextHolder.getContext().getAuthentication().isAuthenticated()));
//            }
//        }
//
        filterChain.doFilter(request, response);
    }
}
