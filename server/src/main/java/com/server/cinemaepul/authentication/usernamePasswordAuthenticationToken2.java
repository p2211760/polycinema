package com.server.cinemaepul.authentication;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class usernamePasswordAuthenticationToken2 extends UsernamePasswordAuthenticationToken {
    public usernamePasswordAuthenticationToken2(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public boolean isAuthenticated(){
        return this.getPrincipal() != null;
    }
}
