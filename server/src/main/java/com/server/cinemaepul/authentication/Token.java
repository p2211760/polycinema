package com.server.cinemaepul.authentication;

import jakarta.persistence.Entity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class Token {
    int id;
    String name;
    String token;
    Date expiration;
}
