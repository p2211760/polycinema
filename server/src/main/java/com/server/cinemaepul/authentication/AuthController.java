package com.server.cinemaepul.authentication;

import com.server.cinemaepul.user.User;
import com.server.cinemaepul.user.UserRepository;
import com.server.cinemaepul.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {


    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @PostMapping("/login")
    public ResponseEntity<Token> authenticateUser(@RequestBody User credentials) {
        System.out.println(credentials.getUsername());
        if (!userRepository.existsByUsername(credentials.getUsername())) {
            return ResponseEntity.status(404).body(null);
        }
        User user = userService.loadByLogin(credentials.getUsername());

        if (!passwordEncoder.matches(credentials.getPassword(), user.getPassword()))
            return ResponseEntity.status(309).body(null);
        String token = jwtUtils.generateJwtToken(user);
        String username = jwtUtils.getUserNameFromJwtToken(token);
        System.out.println(username);
        Token token_object = new Token();
        token_object.token = token;
        token_object.id = user.getId();
        token_object.name = "User";
        token_object.expiration = jwtUtils.getExpiration();
        return ResponseEntity.ok().body(token_object);
    }

    @PostMapping("/sign_up")
    public ResponseEntity<User> registerUser(@RequestBody User user) {
        if (userRepository.existsByUsername(user.getUsername())) {
            throw new ResponseStatusException(
                    HttpStatus.CONFLICT, "User already exist", null);
        }
        return ResponseEntity.ok().body(userService.create(user));
    }
}