package com.server.cinemaepul.movie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FilmRepository extends JpaRepository<Movie, Integer> {
    Movie findByTitle(String titre);

    @Query(value="select * from `movie` where id in (SELECT movie_id from movie_genre where genre_id = ?)", nativeQuery = true)
    List<Movie> getMoviesByCategory(int id);

    //List<Movie> findAllByCodeCat(MovieGenre codeCat);

    //List<Movie> findAllBySearch(@Param("value") String value);


}
