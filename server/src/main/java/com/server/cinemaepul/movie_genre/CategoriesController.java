package com.server.cinemaepul.movie_genre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/CategoriesController")
public class CategoriesController {
    @Autowired
    private CategorieService categorieService;

    @GetMapping
    public List<MovieGenre> findAll() {
        return categorieService.findAll();
    }

}
