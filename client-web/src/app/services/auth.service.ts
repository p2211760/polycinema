import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {Utilisateur} from "../models/utilisateur";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = "http://localhost:8080";
 private _connectedUser: Utilisateur | undefined;
 private _currentUserSubject: BehaviorSubject<Utilisateur | undefined>;
 public currentUser: Observable<Utilisateur | undefined>;


  constructor(private http: HttpClient) {
   this._connectedUser = JSON.parse(localStorage.getItem('connectedUser')!) || undefined;
   this._currentUserSubject = new BehaviorSubject<Utilisateur | undefined>(this._connectedUser);
   this.currentUser = this._currentUserSubject.asObservable();
  }

  login(login: string, password: string): Observable<any> {
    return this.http.post<any>(this.url+'/auth/login', {"username":login,"password":password});
  }

  loginSuccessful(res: JwtResponse): void {
    console.log(res);
    localStorage.setItem('token', res.token);
    localStorage.setItem('id', res.id.toString());
    localStorage.setItem('name', res.name);

    this.me().subscribe({
      next: res => {
        localStorage.setItem('connectedUser', JSON.stringify(Utilisateur.adapt(res)));
        this._connectedUser = Utilisateur.adapt(res);
        this._currentUserSubject.next(this._connectedUser);
      }
    });
  }

  public isAuthenticated(): boolean {
    //localStorage.removeItem('token');
    return localStorage.getItem('token') !== null;
  }

  public getCurrentUser(): Utilisateur | undefined {
    return this._connectedUser;
  }

  public getCurrentUserObservable(): Observable<Utilisateur| undefined>{
    return this.currentUser;
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    this._connectedUser = undefined;
    this._currentUserSubject.next(undefined);
  }

  me(): Observable<Utilisateur> {
    return this.http.get<any>(this.url+'/users/me')
  }
}

export interface JwtResponse {
  id: number;
  name: string;
  token: string;
}
