import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Film } from 'src/app/models/film';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.scss']
})
export class PopUpComponent {

  
  popUp!: boolean;
  @Input() film: Film | undefined;
  @Output() closePopUp: EventEmitter<void> = new EventEmitter<void>();

  onCloseButtonClick() {
    this.closePopUp.emit();
  }

  toHoursAndMinutes(duree: number|undefined) {
    if (duree){
      const hours = Math.floor(duree / 60);
      const minutes = duree % 60;
      return `${hours}h${minutes > 0 ? ` ${minutes}m` : ''}`;
    }
    return '';
  }

  toEuro(number: number|undefined) {
    return number ? new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(number) : '';
  }

  formatDate(date: Date | undefined): string {
    if (!date) return '';    
    const options: Intl.DateTimeFormatOptions = { day: '2-digit', month: 'long', year: 'numeric' };
    return new Date(date).toLocaleDateString('fr-FR', options);
  }
  
  formatNumber(number: number | undefined): string {
    if (!number) return '';

    const absNumber = Math.abs(number);
    const abbreviations = ['', 'K', 'M', 'B', 'T'];
    const index = Math.floor(Math.log10(absNumber) / 3);
    const roundedNumber = absNumber / Math.pow(10, index * 3);
    const formattedNumber = roundedNumber.toFixed(2);

    return (number < 0 ? '-' : '') + formattedNumber + abbreviations[index];
  }

}
