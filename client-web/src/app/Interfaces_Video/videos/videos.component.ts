import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, ParamMap, Router, RouterEvent} from "@angular/router";
import {FilmService} from "../../services/film.service";
import {Film} from "../../models/film";
import {Observable, Subject, takeUntil} from "rxjs";
import {PersonnageAvecFilmDto} from "../../models/personnageAvecFilmDto";
import {Personnage} from "../../models/personnage";
import {PersonnageService} from "../../services/personnage.service";

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit{

  films$: Observable<Film[]> | undefined;
  filmsObserver: any;
  films: Film[] | undefined;
  film: Film | undefined;
  popUp!: boolean;
  showFav: boolean = false;
  codeCat: string|null = null;
  personnages$: Observable<Personnage[]> | undefined;
  personnagesObserver: any;
  personnages: Personnage[] | undefined;
  constructor(private router: Router, private route: ActivatedRoute, private filmService: FilmService, private personnageService: PersonnageService) {
    this.codeCat = this.route.snapshot.queryParamMap.get('codeCat');
  }

  ngOnInit() {
    this.route.queryParamMap.subscribe((paramMap: ParamMap) => {
      this.codeCat = paramMap.get('codeCat');
      if (this.codeCat) {
        this.films$ = this.filmService.getFilmsByCategorie(this.codeCat);
        console.log(this.films$);
      }else{
        this.films$ = this.filmService.getAllFilms();
      }
      this.filmsObserver = {
        next: (f: Film[]) => {
          this.films = f;
        },
        error: (err: Error) => console.error("Error while fetching : " + err),
      }
      this.films$.subscribe(this.filmsObserver)
    });

  }
  
  showPopUp(film: Film) {
    this.popUp = true;
    this.film = film;
  }

  onClosePopUp() {
    this.popUp = false;
  }

}
