import { Realisateur } from './realisateur';
import { Categorie } from './categorie';

export class Film {
  id: number;
  title: string;
  original_title: string;
  original_language: string;
  release_date: Date;
  runtime: number;
  popularity: number;
  poster_path: string;
  backdrop_path: string;
  director_id: number;
  budget: number;
  revenue: number;
  overview: string;
  noRea: Realisateur;
  codeCat: Categorie;

  constructor(id: number, title: string, original_title: string, original_language: string, release_date: Date, runtime: number, popularity: number, poster_path: string, backdrop_path: string, director_id: number, budget: number, revenue: number, overview: string, noRea: Realisateur, codeCat: Categorie) {
    this.id = id;
    this.title = title;
    this.original_title = original_title;
    this.original_language = original_language;
    this.release_date = release_date;
    this.runtime = runtime;
    this.popularity = popularity;
    this.poster_path = poster_path;
    this.backdrop_path = backdrop_path;
    this.director_id = director_id;
    this.budget = budget;
    this.revenue = revenue;
    this.overview = overview;
    this.noRea = noRea;
    this.codeCat = codeCat;
  }

  static adapt(item: any): Film {
    return new Film(
      item.id,
      item.title,
      item.original_title,
      item.original_language,
      new Date(item.release_date),
      item.runtime,
      item.popularity,
      item.poster_path,
      item.backdrop_path,
      item.director_id,
      item.budget,
      item.revenue,
      item.overview,
      item.noRea,
      item.codeCat
    );
  }
}
