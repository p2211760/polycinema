export class Acteur {
  id: number;
  name: string;
  birthday: Date;
  place_of_birth: string;
  death_day: Date | null;
  profile_path: string;
  biography: string;

  constructor(id: number, name: string, birthday: Date, place_of_birth: string, death_day: Date | null, profile_path: string, biography: string) {
    this.id = id;
    this.name = name;
    this.birthday = birthday;
    this.place_of_birth = place_of_birth;
    this.death_day = death_day;
    this.profile_path = profile_path;
    this.biography = biography;
  }

  static adapt(item: any): Acteur {
    return new Acteur(item.id, item.name, item.birthday, item.place_of_birth, item.death_day, item.profile_path, item.biography)
  }
}
