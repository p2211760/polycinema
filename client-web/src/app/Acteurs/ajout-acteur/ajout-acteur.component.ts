import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ActeurService} from "../../services/acteur.service";

@Component({
  selector: 'app-ajout-acteur',
  template: `
    <h1>Ajouter un acteur</h1>
    <div class="form-class">
      <form [formGroup]="form">
        <div class="input-label-div">
          <label>Nom de l'acteur</label>
          <input type="text" name="name" placeholder="Nom de l'acteur" [formControl]="form.controls.name">
        </div>
        <div class="input-label-div">
          <label>Date de naissance</label>
          <input type="date" name="birthday" [formControl]="form.controls.birthday"
                 min="1900-01-01" max="2023-12-31">
        </div>
        <div class="input-label-div">
          <label>Date de décès</label>
          <input type="date" name="death_day" [formControl]="form.controls.death_day"
                 min="1900-01-01" max="2023-12-31">
        </div>
        <div class="input-label-div">
          <label>Lien image</label>
          <input type="text" name="lien_image" placeholder="lien de l'image" [formControl]="form.controls.lienImg">
        </div>
        <button class="btn-submit" (click)="goPageActeurs()">Ajouter</button>
      </form>
    </div>
  `,
  styleUrls: ['./ajout-acteur.component.scss']
})
export class AjoutActeurComponent implements OnInit {
  form = new FormGroup({
    name: new FormControl<string>('', Validators.required),
    birthday: new FormControl<Date>(new Date(), Validators.required),
    death_day: new FormControl<Date>(new Date(), Validators.required),
    lienImg: new FormControl<string>('', Validators.required),
  });

  constructor(private router: Router,
              private acteurService: ActeurService) {
  }

  ngOnInit() {
  }

  goPageActeurs() {
    this.acteurService.addActeur(this.form.value).subscribe(() => {
      this.router.navigateByUrl('acteurs')
    })
  }

}
